## Todo List Webapp  
# Jyotirmoy Mandal  

Implemented a todo list using javascript  
Has the following features:  
- Add item
- Remove item
- Check item
- Reorder items

Implemented local storage and a central data structure in this project  

---

Access the webapp by opening index.html in a web-browser (Preferably Google Chrome)   